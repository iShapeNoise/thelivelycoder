# TheLivelyCoder

This are the project files for the Virtual Reality Club "The Lively Coder", which is published in Mozilla Hubs.

The room is made for Live Coding Events, in which the screens shows visualizations and streams of the code.

![](TheLivelyCoder_Screenshot.png)
